package com.food.ordering.system.order.service.domain.event;

import com.food.ordering.system.domain.event.DomainEvent;

import java.time.ZonedDateTime;

public abstract class OrderEvent implements DomainEvent<OrderEvent> {
    private final com.food.ordering.system.order.service.domain.entity.Order order;
    private final ZonedDateTime createdAt;

    public OrderEvent(com.food.ordering.system.order.service.domain.entity.Order order, ZonedDateTime createdAt) {
        this.order = order;
        this.createdAt = createdAt;
    }

    public com.food.ordering.system.order.service.domain.entity.Order getOrder() {
        return order;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }
}
